# import libraries go here
import random

DEBUG = True
if DEBUG:
    random.seed(100)
    

# global variables go here
game_version = 'easy'
secret = None
guest_count = 4


def game_easy():
    global secret
    secret = random.randint(1,100)
    while True:
        input_num = int(input("Please try a num:"))
        if input_num == secret:
            print("Great...")
            break
        elif input_num > secret:
            print("Too high..")
        else:
            print("Too low..")


def game_hard():
    global secret
    secret = random.randint(1, 100)
    tried_count = 0
    while tried_count < guest_count:
        input_num = int(input("Please try a num:"))
        tried_count += 1
        if input_num == secret:
            print("Great....")
            break
        elif input_num > secret:
            print("Too high..")
        else:
            print("Too low..")
    else:
        print("Answer is:", secret)
        

if __name__ == "__main__":
    while True:
        game_version = input("please choice (E)asy version or (H)ard version:")
        if game_version.lower() == 'e':
            game_easy()
        elif game_version.lower() == 'h':
            game_hard()
        else:
            print("Bye ;)")
            break



